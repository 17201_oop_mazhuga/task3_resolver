cmake_minimum_required(VERSION 3.7)
project(03_resolver)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES Resolver.cpp Resolver.h Version.cpp Version.h Library.cpp Library.h Requirement.cpp Requirement.h)

add_executable(03_resolver main.cpp ${SOURCE_FILES})
add_executable(unittest test.cpp ${SOURCE_FILES})