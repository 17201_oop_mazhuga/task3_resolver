//
// Created by 214 on 27.10.2018.
//

#define VersionRestriction std::pair<vrType, Version>

#ifndef INC_03_RESOLVER_DEPENDENCY_H
#define INC_03_RESOLVER_DEPENDENCY_H

#include "Version.h"

enum class rType {
    Depend, Conflict
};
enum class vrType {
    Less, More, LessOrEqual, MoreOrEqual, Equal, None
};

vrType stringToVersionRestrictionType(std::string &);

class Requirement {
private:
    rType reqType;
    std::string libName;

    vrType verRestType;
    Version version;
public:
    Requirement();
    Requirement(const rType &, const std::string &, const vrType &, const Version &);
    Requirement(const rType &, const std::string &, const vrType & = vrType::None);

    rType getType() const;
    vrType getVersionRestriction() const;
    std::string getLibName() const;
    Version getVersion() const;

    bool operator==(const Requirement &other) const;
    bool operator!=(const Requirement &other) const;
};


#endif //INC_03_RESOLVER_DEPENDENCY_H
