//
// Created by d.mazhuga on 15.10.2018.
//
#include <map>
#include <set>
#include <fstream>
#include <iostream>
#include <exception>

#ifndef INC_02_TRITSET_RESOLVER_H
#define INC_02_TRITSET_RESOLVER_H

#include "Library.h"

class Resolver {
private:
    std::vector<Library> libs;
    std::vector<Requirement> targets;

    bool isNextLineARequirement(std::ifstream &stream) const;        //function for parser

    bool doesFitRequirement(const Library &l, const Requirement &req) const;
    bool doesFitCompatibility(const Library &l, const Requirement &req) const;

    bool doesFitRequirement(const Library &l, const std::vector<Requirement> &reqVector) const;
    bool doesFitCompatibility(const Library &l, const std::vector<Requirement> &reqVector) const;

    bool doesFitRequirement(const std::set<Library> &libSet, const Requirement &req) const;

    bool checkCompatibility(const Library &l1, const Library &l2) const;
    bool checkCompatibility(const Library &l, const std::set<Library> &libSet) const;

    Requirement getOptimalTarget(std::vector<Library> &baseLibs) const;

    void recursiveResolutionSearch(std::map<std::string, std::vector<Requirement>> dependList, const Library &baseLib,
                                       const Library &curLib, std::set<Library> resolution,
                                       std::set<std::set<Library>> &resolutionsList);
public:
    Resolver();

    void parse(std::ifstream &libsStream, std::ifstream &targetsStream);
    std::set<std::set<Library>> getAllResolutions();
    void printAllResolutions(std::ostream &out = std::cout);

    std::set<Library> getBestResolution();
    void printBestResolution(std::ostream &out = std::cout);

    std::vector<Library> getLibraries() const;
    std::vector<Requirement> getTargets() const;
};


#endif //INC_02_TRITSET_RESOLVER_H
