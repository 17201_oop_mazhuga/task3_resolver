//
// Created by 214 on 27.10.2018.
//

#include <string>
#include <vector>
#include <iostream>

#ifndef INC_03_RESOLVER_LIBRARY_H
#define INC_03_RESOLVER_LIBRARY_H

#include "Version.h"
#include "Requirement.h"

class Library {
private:
    std::string name;
    Version version;
    std::vector<Requirement> requirements;
public:
    Library();
    Library(const std::string &nm, const Version &ver);

    void setName(const std::string &nm);
    void setVersion(const Version &ver);
    void addRequirement(const Requirement &d);
    std::string getName() const;
    Version getVersion() const;
    std::vector<Requirement> getRequirements() const;

    friend std::ostream& operator<<(std::ostream &out, const Library l);

    bool operator==(const Library &other) const;
    bool operator!=(const Library &other) const;
    bool operator<(const Library &other) const;
    bool operator>(const Library &other) const;
    bool operator<=(const Library &other) const;
    bool operator>=(const Library &other) const;
};


#endif //INC_03_RESOLVER_LIBRARY_H
