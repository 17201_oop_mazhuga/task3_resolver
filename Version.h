//
// Created by d.mazhuga on 15.10.2018.
//

#include <vector>
#include <string>

#ifndef INC_02_TRITSET_VERSION_H
#define INC_02_TRITSET_VERSION_H

class Version {
private:
    std::vector<unsigned int> numberArray;
public:
    Version();
    Version(const std::string &s);
    Version(const char* s);

    std::string toString() const;
    friend std::ostream& operator<<(std::ostream &out, const Version v);

    unsigned int operator[](size_t index) const;

    bool operator==(const Version &other) const;
    bool operator!=(const Version &other) const;
    bool operator<(const Version &other) const;
    bool operator>(const Version &other) const;
    bool operator<=(const Version &other) const;
    bool operator>=(const Version &other) const;
};


#endif //INC_02_TRITSET_VERSION_H
