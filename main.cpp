//
// Created by d.mazhuga on 15.10.2018.
//

#include <iostream>

#include "Resolver.h"

int main() {
    Resolver resolver;
    std::ifstream libs, targets;

    libs.open("libs.txt");
    targets.open("targets.txt");

    resolver.parse(libs, targets);
    //resolver.printAllResolutions();
    //std::cout << "=================================\n\n";
    try {
        resolver.printBestResolution();
    }
    catch (std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        exit(1);
    }

    return 0;
}