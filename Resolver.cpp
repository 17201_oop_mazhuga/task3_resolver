//
// Created by d.mazhuga on 15.10.2018.
//

#include "Resolver.h"

Resolver::Resolver() {
}


bool Resolver::isNextLineARequirement(std::ifstream &stream) const {
    if (stream.peek() == '\n')              //linux line end
        stream.get();
    else if (stream.peek() == '\r') {       //windows line end
        stream.get();
        stream.get();
    }
    return stream.peek() == '+' || stream.peek() == '-';
}

void Resolver::parse(std::ifstream &libsStream, std::ifstream &targetsStream) {
    std::string tempString;
    Version tempVersion;
    Library tempLibrary;
    Requirement tempRequirement;
    rType tempReqType;
    vrType tempVerReqType;
    std::string tempName;

    //TODO: exceptions if input has wrong format

    while (libsStream.good() && libsStream >> tempString) {
        tempName = tempString;

        libsStream >> tempString;
        tempVersion = Version(tempString);

        if (!libsStream.good()) {
            libs.emplace_back(tempName, tempVersion);
            break;
        }

        tempLibrary = Library(tempName, tempVersion);

        while (isNextLineARequirement(libsStream)) {
            libsStream >> tempString;
            if (tempString == "+")
                tempReqType = rType::Depend;
            else
                tempReqType = rType::Conflict;

            libsStream >> tempString;
            tempName = tempString;

            if (!libsStream.good()) {
                tempLibrary.addRequirement(Requirement(tempReqType, tempName));
                break;
            }
            if (libsStream.peek() == '\n' || libsStream.peek() == '\r') {
                tempLibrary.addRequirement(Requirement(tempReqType, tempName));
                continue;
            }

            libsStream >> tempString;
            tempVerReqType = stringToVersionRestrictionType(tempString);

            libsStream >> tempString;
            tempVersion = Version(tempString);

            tempLibrary.addRequirement(Requirement(tempReqType, tempName, tempVerReqType, tempVersion));
        }

        libs.push_back(tempLibrary);
    }

    libsStream.close();

    while (targetsStream.good() && targetsStream >> tempString) {
        tempName = tempString;

        if (!targetsStream.good()) {
            targets.emplace_back(rType::Depend, tempName);
            break;
        }
        if (targetsStream.peek() == '\n' || targetsStream.peek() == '\r') {
            targets.emplace_back(rType::Depend, tempName);
            continue;
        }

        targetsStream >> tempString;
        tempVerReqType = stringToVersionRestrictionType(tempString);

        targetsStream >> tempString;
        tempVersion = Version(tempString);

        targets.emplace_back(rType::Depend, tempName, tempVerReqType, tempVersion);
    }

    targetsStream.close();
}

std::vector<Library> Resolver::getLibraries() const {
    return libs;
}

std::vector<Requirement> Resolver::getTargets() const {
    return targets;
}

Requirement Resolver::getOptimalTarget(std::vector<Library> &baseLibs) const {
    Requirement returnTarget;
    unsigned int maxVersionsCounter = 0;
    unsigned int curVersionsCounter = 0;

    for (const Requirement &tar : targets) {
        for (const Library &i : libs) {
            if (doesFitRequirement(i, tar)) {
                curVersionsCounter++;
                baseLibs.push_back(i);
            }
        }

        if (curVersionsCounter > maxVersionsCounter) {
            maxVersionsCounter = curVersionsCounter;
            returnTarget = tar;
        }

        curVersionsCounter = 0;
    }

    return returnTarget;
}

std::set<std::set<Library>> Resolver::getAllResolutions() {
    std::set<std::set<Library>> returnResolutionsList;
    std::vector<Library> baseLibs;

    //base requirement is a target that has less libs that fit it
    //base libs are libs that fit the optimal target
    Requirement baseReq = getOptimalTarget(baseLibs);

    //set to store current state of resolution
    std::set<Library> resolution;

    //to merge requirements with same lib name i use map of vectors
    //key is the name of the lib
    //here dependencies that are not yet satisfied are stored
    std::map<std::string, std::vector<Requirement>> dependList;

    //add all targets (except the base) to list of dependencies
    for (const Requirement &req : targets)
        if (req != baseReq)
            dependList[req.getLibName()].push_back(req);

    //run recursive search for each base lib
    for (const Library &i : baseLibs)
        recursiveResolutionSearch(dependList, i, i,
                                  resolution, returnResolutionsList);

    return returnResolutionsList;
}

void
Resolver::recursiveResolutionSearch(std::map<std::string, std::vector<Requirement>> dependList, const Library &baseLib,
                                    const Library &curLib, std::set<Library> resolution,
                                    std::set<std::set<Library>> &resolutionsList) {
    resolution.insert(curLib);

    for (const Requirement &req : curLib.getRequirements()) {

        //dependency is added only if it is not already satisfied

        if (req.getType() == rType::Depend  && !doesFitRequirement(resolution, req))
            dependList[req.getLibName()].push_back(req);
    }

    if (dependList.empty()) {
            resolutionsList.insert(resolution);
            return;
    }

    for (const auto &currentDependency : dependList) {
        const std::vector<Requirement> &reqVector = currentDependency.second;
        for (const Library &l : libs) {
            if (checkCompatibility(l, resolution) && doesFitRequirement(l, reqVector)) {

                //have to delete dependency before recursion, but it should be there after
                //so i just make new dependency list, without current dependency

                std::map<std::string, std::vector<Requirement>> newDependList;
                for (const auto &i : dependList)
                    if (i != currentDependency)
                        newDependList.insert(i);

                recursiveResolutionSearch(newDependList, baseLib, l,
                                          resolution, resolutionsList);
            }
        }
    }
}

bool Resolver::doesFitRequirement(const Library &l, const Requirement &req) const {

    //return true if type == Depend and lib fits
    //or if type == Conflict and lib DOES NOT fit the conflict

    if (req.getType() == rType::Depend) {
        if (l.getName() == req.getLibName()) {
            if (req.getVersionRestriction() == vrType::None)
                return true;
            else if (req.getVersionRestriction() == vrType::Less && l.getVersion() < req.getVersion())
                return true;
            else if (req.getVersionRestriction() == vrType::More && l.getVersion() > req.getVersion())
                return true;
            else if (req.getVersionRestriction() == vrType::LessOrEqual && l.getVersion() <= req.getVersion())
                return true;
            else if (req.getVersionRestriction() == vrType::MoreOrEqual && l.getVersion() >= req.getVersion())
                return true;
            else if (req.getVersionRestriction() == vrType::Equal && l.getVersion() == req.getVersion())
                return true;
        }

        return false;
    }
    else {
        if (l.getName() == req.getLibName()) {
            if (req.getVersionRestriction() == vrType::None)
                return false;
            else if (req.getVersionRestriction() == vrType::Less && l.getVersion() < req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::More && l.getVersion() > req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::LessOrEqual && l.getVersion() <= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::MoreOrEqual && l.getVersion() >= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::Equal && l.getVersion() == req.getVersion())
                return false;
        }

        return true;
    }
}

bool Resolver::doesFitCompatibility(const Library &l, const Requirement &req) const {

    //like doesFitRequirement but can be true if it is not a dependency

    if (req.getType() == rType::Depend) {
        if (l.getName() == req.getLibName()) {
            if (req.getVersionRestriction() == vrType::None)
                return true;
            else if (req.getVersionRestriction() == vrType::Less && l.getVersion() >= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::More && l.getVersion() <= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::LessOrEqual && l.getVersion() > req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::MoreOrEqual && l.getVersion() < req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::Equal && l.getVersion() != req.getVersion())
                return false;
        }

        return true;
    }
    else {
        if (l.getName() == req.getLibName()) {
            if (req.getVersionRestriction() == vrType::None)
                return false;
            else if (req.getVersionRestriction() == vrType::Less && l.getVersion() < req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::More && l.getVersion() > req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::LessOrEqual && l.getVersion() <= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::MoreOrEqual && l.getVersion() >= req.getVersion())
                return false;
            else if (req.getVersionRestriction() == vrType::Equal && l.getVersion() == req.getVersion())
                return false;
        }

        return true;
    }
}

bool Resolver::checkCompatibility(const Library &l1, const Library &l2) const {
    if (l1.getName() == l2.getName())
        return false;

    for (const Requirement &req : l1.getRequirements()) {
        if (!doesFitCompatibility(l2, req))
            return false;
    }
    for (const Requirement &req : l2.getRequirements()) {
        if (!doesFitCompatibility(l1, req))
            return false;
    }

    return true;
}

bool Resolver::doesFitRequirement(const Library &l, const std::vector<Requirement> &reqVector) const {

    //returns true if the lib fits all requirements

    for (const Requirement &req : reqVector)
        if (!doesFitRequirement(l, req))
            return false;

    return true;
}

bool Resolver::doesFitCompatibility(const Library &l, const std::vector<Requirement> &reqVector) const {

    //returns true if the lib fits all compatibilities

    for (const Requirement &req : reqVector)
        if (!doesFitCompatibility(l, req))
            return false;

    return true;
}

bool Resolver::doesFitRequirement(const std::set<Library> &libSet, const Requirement &req) const {

    //return true if at least one lib fits requirement

    for (const Library &l : libSet)
        if (doesFitRequirement(l, req))
            return true;

    return false;
}

bool Resolver::checkCompatibility(const Library &l, const std::set<Library> &libSet) const {

    //returns true if l is compatible with all libs from set

    for (const Library &i : libSet)
        if (!checkCompatibility(l, i))
            return false;

    return true;
}


void Resolver::printAllResolutions(std::ostream &out) {
    std::set<std::set<Library>> resolutionsSet = getAllResolutions();

    if (resolutionsSet.empty()) {
        throw std::runtime_error("Unable to resolve targets.");
    }
    else {
        int i = 0;
        for (const auto &res : resolutionsSet) {
            i++;
            out << "Resolution " << i << ":" << std::endl;
            for (const Library &lib : res)
                out << lib << std::endl;

            out << std::endl;
        }
    }
}

std::set<Library> Resolver::getBestResolution() {
    std::set<std::set<Library>> resolutionsSet = getAllResolutions();

    if (resolutionsSet.empty())
        return std::set<Library>();

    return *(resolutionsSet.rbegin());
}

void Resolver::printBestResolution(std::ostream &out) {
    std::set<Library> resolution = getBestResolution();

    if (resolution.empty()) {
        throw std::runtime_error("Unable to resolve targets.");
    }
    else {
        for (const Library &l : resolution)
            out << l << std::endl;
    }
}