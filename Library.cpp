#include <utility>

#include <utility>

#include <utility>

//
// Created by 214 on 27.10.2018.
//

#include "Library.h"

Library::Library(const std::string &nm, const Version &ver) : name(nm), version(ver) {

}

Library::Library() = default;

void Library::setName(const std::string &nm) {
    name = nm;
}

void Library::setVersion(const Version &ver) {
    version = ver;
}

void Library::addRequirement(const Requirement &d) {
    requirements.push_back(d);
}

std::string Library::getName() const {
    return name;
}

Version Library::getVersion() const {
    return version;
}

std::vector<Requirement> Library::getRequirements() const {
    return requirements;
}

bool Library::operator==(const Library &other) const {
    return name == other.name && version == other.version;

}

bool Library::operator!=(const Library &other) const {
    return !((*this) == other);
}

bool Library::operator<(const Library &other) const {
    return name < other.name || (name == other.name && version < other.version);
}

bool Library::operator>(const Library &other) const {
    return name > other.name || (name == other.name && version > other.version);
}

bool Library::operator<=(const Library &other) const {
    return !((*this) > other);
}

bool Library::operator>=(const Library &other) const {
    return !((*this) < other);
}

std::ostream &operator<<(std::ostream &out, const Library l) {
    out << l.getName() << " " << l.getVersion();
    return out;
}
