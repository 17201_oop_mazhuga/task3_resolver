#include <utility>

//
// Created by 214 on 27.10.2018.
//

#include "Requirement.h"

Requirement::Requirement(const rType &rt, const std::string &name, const vrType &vrt, const Version &ver) : reqType(rt),
                                                                                                            verRestType(
                                                                                                                    vrt),
                                                                                                            libName(name),
                                                                                                            version(ver) {}

Requirement::Requirement(const rType &rt, const std::string &name, const vrType &vrt) : reqType(rt), verRestType(vrt),
                                                                                        libName(name),
                                                                                        version("0") {

}

Requirement::Requirement() : reqType(rType::Depend), verRestType(vrType::None) {

}

rType Requirement::getType() const {
    return reqType;
}

std::string Requirement::getLibName() const {
    return libName;
}

vrType Requirement::getVersionRestriction() const {
    return verRestType;
}

Version Requirement::getVersion() const {
    return version;
}

bool Requirement::operator==(const Requirement &other) const {
    return reqType == other.reqType && verRestType == other.verRestType && libName == other.libName &&
           version == other.version;
}

bool Requirement::operator!=(const Requirement &other) const {
    return !((*this) == other);
}

vrType stringToVersionRestrictionType(std::string &s) {
    if (s == "<")
        return vrType::Less;
    if (s == ">")
        return vrType::More;
    if (s == "<=")
        return vrType::LessOrEqual;
    if (s == ">=")
        return vrType::MoreOrEqual;
    if (s == "==")
        return vrType::Equal;

    return vrType::None;
}