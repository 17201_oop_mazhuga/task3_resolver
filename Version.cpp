//
// Created by d.mazhuga on 15.10.2018.
//

#include "Version.h"

Version::Version() {
    numberArray.push_back(0);
}

Version::Version(const std::string &s) {
    std::string tempString;

    if (s.empty())
        numberArray.push_back(0);

    else {
        for (char i : s) {
            if (i == '.') {
                numberArray.push_back(stoul(tempString));
                tempString = "";
            } else
                tempString += i;
        }
        numberArray.push_back(stoul(tempString));
    }
}

Version::Version(const char *s) {
    std::string newStr(s);
    Version v(newStr);
    (*this) = v;
}

std::string Version::toString() const {
    std::string str;

    for (int i = 0; i < numberArray.size(); i++) {
        if (i != 0)
            str += '.';
        str += std::to_string(numberArray[i]);
    }

    return str;
}

std::ostream &operator<<(std::ostream &out, const Version v) {
    out << v.toString();
    return out;
}

unsigned int Version::operator[](size_t index) const {
    if (index >= numberArray.size())
        return 0;
    else
        return numberArray[index];
}

bool Version::operator==(const Version &other) const {
    size_t biggerLength = (numberArray.size() >= other.numberArray.size()) ? numberArray.size()
                                                                           : other.numberArray.size();
    for (size_t i = 0; i < biggerLength; i++) {
        if ((*this)[i] != other[i])
            return false;
    }

    return true;
}

bool Version::operator!=(const Version &other) const {
    return !((*this) == other);
}

bool Version::operator<(const Version &other) const {
    size_t biggerLength = (numberArray.size() >= other.numberArray.size()) ? numberArray.size()
                                                                           : other.numberArray.size();
    for (size_t i = 0; i < biggerLength; i++) {
        if ((*this)[i] < other[i])
            return true;
        else if ((*this)[i] > other[i])
            return false;
    }

    return false;
}

bool Version::operator>(const Version &other) const {
    size_t biggerLength = (numberArray.size() >= other.numberArray.size()) ? numberArray.size()
                                                                           : other.numberArray.size();
    for (size_t i = 0; i < biggerLength; i++) {
        if ((*this)[i] > other[i])
            return true;
        else if ((*this)[i] < other[i])
            return false;
    }

    return false;
}

bool Version::operator<=(const Version &other) const {
    return !((*this) > other);
}

bool Version::operator>=(const Version &other) const {
    return !((*this) < other);
}