//
// Created by d.mazhuga on 15.10.2018.
//

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <iostream>
#include <string>

#include "catch.hpp"
#include "Resolver.h"

TEST_CASE("Version constructors and comparison") {
    std::string s1 = "1.2.3";
    Version v1(s1);
    Version v2("1.3.2.1");

    REQUIRE(v1 < v2);
    REQUIRE(v2 > v1);
    REQUIRE(v1 <= v2);
    REQUIRE(v2 >= v1);
    REQUIRE(v1 != v2);
    REQUIRE(v1 == v1);
    REQUIRE(v2 == v2);

    v1 = "1.1.0.679";
    v2 = "1.1.0.679";

    REQUIRE(v1 == v2);
    REQUIRE(v1 <= v2);
    REQUIRE(v2 >= v1);
    REQUIRE(!(v1 < v2));
    REQUIRE(!(v2 > v1));

    v2 = Version();         // == "0"

    REQUIRE(v1 > v2);
    REQUIRE(v2 == "0");
}
TEST_CASE("Version operator[]") {
    Version v = "1.2";

    REQUIRE(v[0] == 1);
    REQUIRE(v[1] == 2);
    REQUIRE(v[3] == 0);
}
TEST_CASE("Version toString() and operator<<") {
    std::string s = "1.2.3";
    Version v(s);

    REQUIRE(v.toString() == s);

    std::stringstream ss;
    ss << v;

    REQUIRE(ss.str() == s);
}

TEST_CASE("Requirement constructors and comparison") {
    Requirement r1(rType::Depend, "B");
    Requirement r2(rType::Depend, "B", vrType::Equal, "1.0");

    REQUIRE(r1 != r2);

    r2 = Requirement(rType::Depend, "B", vrType::None);

    REQUIRE(r1 == r2);

    r2 = Requirement();

    REQUIRE(r1 != r2);
}
TEST_CASE("Requirement getters") {
    rType reqType = rType::Conflict;
    std::string libName = "D";
    vrType verResType = vrType::MoreOrEqual;
    Version ver = "2.3";

    Requirement r(reqType, libName, verResType, ver);

    REQUIRE(r.getType() == reqType);
    REQUIRE(r.getLibName() == libName);
    REQUIRE(r.getVersionRestriction() == verResType);
    REQUIRE(r.getVersion() == ver);
}

TEST_CASE("Library constructors and comparison") {
    Library l1("A", "1.0");
    Library l2("A", "1.0.1");

    REQUIRE(l1 < l2);
    REQUIRE(l2 > l1);
    REQUIRE(l1 <= l2);
    REQUIRE(l2 >= l1);
    REQUIRE(l1 != l2);
    REQUIRE(l1 == l1);
    REQUIRE(l2 == l2);

    l2 = Library("A", "1.0");

    REQUIRE(l1 == l2);
    REQUIRE(l1 <= l2);
    REQUIRE(l2 >= l1);
    REQUIRE(!(l1 < l2));
    REQUIRE(!(l2 > l1));

    l2 = Library("B", "2.8");

    REQUIRE(l1 < l2);
    REQUIRE(l2 > l1);
    REQUIRE(l1 <= l2);
    REQUIRE(l2 >= l1);
    REQUIRE(l1 != l2);

    l2 = Library();             // == "", "0"

    REQUIRE(l1 > l2);
}
TEST_CASE("Library setters and getters") {
    Library l;
    std::string name = "A";
    Version ver = "1.0";
    Requirement req(rType::Depend, "B");

    l.setName(name);
    l.setVersion(ver);
    l.addRequirement(req);

    REQUIRE(l.getName() == name);
    REQUIRE(l.getVersion() == ver);
    REQUIRE(l.getRequirements() == std::vector<Requirement>{req});
}
TEST_CASE("Library operator<<") {
    Library l("A", "1.0");
    std::stringstream out;
    out << l;

    REQUIRE(out.str() == "A 1.0");
}

TEST_CASE("Resolver parse() and getters") {
    std::vector<Library> reqLibs;
    std::vector<Requirement> reqTargets;

    Library tempLibrary("A", "1.0");
    tempLibrary.addRequirement(Requirement(rType::Depend, "B", vrType::MoreOrEqual, "1.0"));
    reqLibs.push_back(tempLibrary);

    reqLibs.emplace_back("B", "0.9");
    reqLibs.emplace_back("B", "1.1");

    tempLibrary = Library("B", "1.2");
    tempLibrary.addRequirement(Requirement(rType::Conflict, "A", vrType::None));
    reqLibs.push_back(tempLibrary);

    reqTargets.emplace_back(rType::Depend, "A", vrType::More, "0.0");

    std::ifstream libsStream, targetsStream;
    libsStream.open("tests/00libs.txt");
    targetsStream.open("tests/00targets.txt");

    Resolver resolver;
    resolver.parse(libsStream, targetsStream);

    REQUIRE(reqLibs == resolver.getLibraries());
    REQUIRE(reqTargets == resolver.getTargets());
}

TEST_CASE("Full Test 0", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/00libs.txt");
    targets.open("tests/00targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.0\n"
                         "B 1.1\n");
}

TEST_CASE("Full Test 1", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/01libs.txt");
    targets.open("tests/01targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 2.0\n");
}

TEST_CASE("Full Test 2", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/02libs.txt");
    targets.open("tests/02targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.0\n"
                         "B 2.0\n");
}

TEST_CASE("Full Test 3", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/03libs.txt");
    targets.open("tests/03targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.0\n"
                         "B 2.0\n");
}

TEST_CASE("Full Test 4", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/04libs.txt");
    targets.open("tests/04targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.5\n");
}

TEST_CASE("Full Test 5", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/05libs.txt");
    targets.open("tests/05targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.1\n"
                         "B 1.1\n"
                         "C 3.0\n");
}

TEST_CASE("Full Test 6", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/06libs.txt");
    targets.open("tests/06targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.1\n"
                         "B 1.1\n"
                         "D 1.5\n");
}

TEST_CASE("Full Test 7", "from avoronkov.gitlab.io") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("tests/07libs.txt");
    targets.open("tests/07targets.txt");

    resolver.parse(libs, targets);
    try {
        resolver.printBestResolution(out);
    }
    catch (std::runtime_error &err) {
        std::string ret = "Unable to resolve targets.";
        REQUIRE(ret == err.what());
    }
}

TEST_CASE("Full Test m1") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("my_tests/01libs.txt");
    targets.open("my_tests/01targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.0\n"
                         "B 2.0\n");
}

TEST_CASE("Full Test m2") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("my_tests/02libs.txt");
    targets.open("my_tests/02targets.txt");

    resolver.parse(libs, targets);
    resolver.printBestResolution(out);

    REQUIRE(out.str() == "A 1.0\n"
                         "B 1.0\n"
                         "C 2.0\n");
}

TEST_CASE("Full Test m3") {
    Resolver resolver;

    std::stringstream out;
    std::ifstream libs, targets;

    libs.open("my_tests/03libs.txt");
    targets.open("my_tests/03targets.txt");

    resolver.parse(libs, targets);
    try {
        resolver.printBestResolution(out);
    }
    catch (std::runtime_error &err) {
        std::string ret = "Unable to resolve targets.";
        REQUIRE(ret == err.what());
    }
}